
import { User } from "../models/usermodel";
import { userReducer } from "./Reducers/UsersReducer";
import { NgRedux } from "@angular-redux/store";
import { combineReducers, Reducer } from "redux";
import { Actions } from "./Actions/Actions";

export interface IAppState {
    user: any

}

export const InitialState: IAppState = {
    user: null
}

export const rootReducer = combineReducers<IAppState>({ user: userReducer });