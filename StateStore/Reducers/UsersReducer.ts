import { Actions } from "../Actions/Actions";
import { tassign } from "tassign";
import { IAppState } from "../../StateStore/Store";

export function userReducer(state:IAppState, action: { type: Actions, payload: any }): any {

    switch (action.type) {
        case Actions.USER_AUTHENTICATED:
            return tassign(state, { user: action.payload });
        default:
            return state;

    }
}

