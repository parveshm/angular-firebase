import { OnDestroy, OnInit } from "@angular/core"
import { Subscription } from "rxjs/Subscription";
export class BaseComponent implements OnDestroy, OnInit {

    protected subscriptions: Subscription[] = [];

    ngOnInit(): void {

    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }


}