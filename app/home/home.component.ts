
import { Component } from "@angular/core";
import { NgRedux } from "@angular-redux/store";
import { IAppState } from "../../StateStore/Store";
import { UserService } from "../../Services/UserService";
import { ExpenseModel } from "../../models/expensemodel";
import { Subject } from "rxjs/Subject";

@Component({
    selector: "user-home",
    templateUrl: "./home.html"
})
export class HomeComponent {
    public expense: ExpenseModel = { when:'', amount: 0.00,title:'' };
    public error: Subject<string> = new Subject<string>();
    constructor(private ngRedux: NgRedux<IAppState>, private userService: UserService) {

    }

    public addExpenses(): void {

        if (this.expense.amount > 0 && this.expense.when != null) {

            console.log(this.expense);

            this.userService.CreateExpense(this.expense);

        }

    }
}
