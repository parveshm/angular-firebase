import { AppComponent } from "./app.component";
import { TestBed, async } from "@angular/core/testing";
import { UserService } from "../Services/UserService";
import { AngularFire, AngularFireModule } from "angularfire2";

describe('AppComponent', () => {

    beforeEach(() => {
        let firebaseConfig = {
            apiKey: "AIzaSyCCE6cIsKJC_4W3ruFMEGMVz93nCXI15iw",
            authDomain: "logsreader-3a436.firebaseapp.com",
            databaseURL: "https://logsreader-3a436.firebaseio.com",
            storageBucket: "logsreader-3a436.appspot.com",
            messagingSenderId: "1009410504127"
        };
        TestBed.configureTestingModule({
            declarations: [AppComponent],
            imports: [AngularFireModule.initializeApp(firebaseConfig)],
            providers: [UserService]
        });

        it('Should Create component and check message', async(() => {

            let fixture = TestBed.createComponent(AppComponent);

            expect(fixture).toBeFalsy();

        }));

    });

});


