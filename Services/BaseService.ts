
import { ILogger } from "./ILogger";
import { Observable, Subject } from "rxjs";
import { Error } from "./Errors";


export class BaseService implements ILogger {

    public messages: Subject<string>;
    public errorMessages: Subject<string>;
    private _emitErrors: boolean = true;

    constructor() {
        if (process.env.ENV == 'production') {
            this._emitErrors = false;
        }
    }

    onError = (error: any): void => {

        this.errorMessages.next(error.message || error);
        this.log(error);
        throw RangeError(error.message || error);

    };


    log = (message: any): void => {

        console.log(message);
    }
    settings = (config: any): void => {
        //Nothing here : TODO
    }




}