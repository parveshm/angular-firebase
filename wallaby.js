module.exports = function (wallaby) {
  return {
    files: [
      { pattern: 'node_modules/phantomjs-polyfill/bind-polyfill.js', instrument: false },
      { pattern: 'node_modules/es6-shim/es6-shim.js', instrument: false },
      { pattern: 'node_modules/reflect-metadata/Reflect.js', instrument: false },
      { pattern: 'app/**.ts', load: false },
      { pattern: './**.ts', load: false }

    ],
    tests: [
      { pattern: 'app/*.spec.ts', load: true }
    ],
    compilers: {
      'app/**/*.ts': wallaby.compilers.typeScript({
        "emitDecoratorMetadata": true,
        "experimentalDecorators": true,
        "noImplicitAny": false
      })
    },
    debug: true, testFramework: 'jasmine'
  };
}
